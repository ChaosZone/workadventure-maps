# ChaosZone workadventure maps

This repository contains links to the [ChaosZone](https://chaoszone.cz/) maps and tilesets for [workadventure](https://workadventu.re/).

## license

The files in this repository are (unless otherwise stated) licensed under Creative Commons CC0 1.0 Universal.
